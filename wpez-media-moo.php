<?php
/*
Plugin Name: WPezPlugins: Media Mirror Originals Only
Plugin URI: https://gitlab.com/WPezPlugins/wpez-media-mirror-originals-only
Description: TODO
Version: 0.0.2
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: http://AlchemyUnited.com
License: GPLv2+
Text Domain: wpez-mmoo
Domain Path: /languages
*/

namespace WPezMediaMirrorOriginalsOnly;

require_once 'ClassParseWPURL.php';

class ClassPlugin {

	//protected $_str_uploads_path;
	//protected $_str_uploads_subdir;
	protected $_str_folder_slug;
	protected $_new_parse_wp_url;


	public function __construct() {

		$this->setPropertyDefaults();

		// add_filter( 'upload_dir', [ $this, 'uploadDirCB' ] );

		add_action( 'add_attachment', [ $this, 'addAttachmentCB' ] );

		add_action( 'before_delete_post', [ $this, 'beforeDeletePostCB' ] );
		add_action( 'delete_post', [ $this, 'beforeDeletePostCB' ] );

	}

	protected function setPropertyDefaults(){

		$this->_str_folder_slug = '-origs-only';
		$this->_new_parse_wp_url = new ClassParseWPURL();

	}

	// array $uploads Array of upload directory data with keys of 'path', 'url', 'subdir, 'basedir', and 'error'.
	// https://developer.wordpress.org/reference/functions/wp_upload_dir/
	// problem is: wp_upload_dir() (might) creats a folder whether you need it or not. this is a workaround ;)
	public function uploadDirCB( $uploads ) {

		if ( is_array( $uploads ) ) {

			$this->_str_uploads_path   = $uploads['path'];
			$this->_str_uploads_subdir = $uploads['subdir'];

		}

		return $uploads;
	}

	public function beforeDeletePostCB( $post_id ) {

		$this->mirrorCRUD( $post_id, 'delete' );


	}

	public function addAttachmentCB( $post_id ) {

		$this->mirrorCRUD( $post_id, 'add' );


	}

	// TODO - split into 3+ methods
	protected function mirrorCRUD( $post_id, $str_crud = false ) {

		if ( $str_crud != 'add' && $str_crud != 'delete' ) {
			return 'error - 1';
		}

		$str_url = wp_get_attachment_url( $post_id );

		if ( $str_url === false ) {
			return 'error - 2';
		}

		$bool_set_url = $this->_new_parse_wp_url->setURL($str_url );

		if ( $bool_set_url === false ){
			return ' error - 2b';
		}

		// is the slug being changed?
		$str_folder_slug = $this->_str_folder_slug;
		$new_slug = apply_filters('wpez_media_mirror_originals_only_folder_slug', false, $post_id);
		if ( is_string($new_slug) && ! empty($new_slug) ){
			// TODO - add trim(), etc. or completely trust the filter?
			$str_folder_slug = $new_slug;
		}

		$arr_upload_dir[] = $this->_new_parse_wp_url->abspath;
		$arr_upload_dir[] = $this->_new_parse_wp_url->uploads . $str_folder_slug;
		$arr_upload_dir[] = $this->_new_parse_wp_url->uploads_subdir;

		$str_upload_dir = implode('/', $arr_upload_dir);
		$str_file_ext = $this->_new_parse_wp_url->filename . '.' . $this->_new_parse_wp_url->extension;

		if ( $str_crud == 'add'){

			$bool_mkdir = true;
			// do we need to create the folder?
			if ( ! file_exists( $str_upload_dir )) {
				// TODO - add filter for permissions?
				$bool_mkdir = mkdir( $str_upload_dir, 0777, true );
			}
			if ($bool_mkdir === true ) {
				// copy the file over
				return copy( $str_url, $str_upload_dir . '/' . $str_file_ext );
			}
			return false;
		}

		if ( $str_crud == 'delete'){

			if ( file_exists( $str_upload_dir . '/' . $str_file_ext )) {

				return wp_delete_file($str_upload_dir . '/' . $str_file_ext);
			}
			return false;

		}
		return 'error - z';


	}

}

new ClassPlugin();