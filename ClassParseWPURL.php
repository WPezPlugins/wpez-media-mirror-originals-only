<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 3/31/2019
 * Time: 3:34 PM
 */

namespace WPezMediaMirrorOriginalsOnly;

class ClassParseWPURL {

	protected $_str_url;
	protected $_str_scheme;
	protected $_str_host;
	protected $_str_uploads;
	protected $_str_uploads_subdir;
	protected $_str_filename;
	protected $_str_extension;
	protected $_str_query;
	protected $_str_fragment;

	public function __construct() {

		$this->_str_url;

	}

	protected function setPropertyDefaults() {

		$this->_str_url = false;

	}


	public function setURL( $str_url = false ) {

		if ( filter_var( $str_url, FILTER_VALIDATE_URL ) ) {

			if ( $str_url != $this->_str_url) {
				$this->_str_url = $str_url;
				$this->resetProperties();
				$this->setProperties( $str_url );
			}
			return true;
		}

		return false;

	}

	protected function resetProperties(){

		$this->_str_scheme = false;
		$this->_str_host = false;
		$this->_str_uploads = false;
		$this->_str_uploads_subdir = false;
		$this->_str_filename = false;
		$this->_str_extension = false;
		$this->_str_query = false;
		$this->_str_fragment = false;
	}

	protected function setProperties( $str_url = false ) {

		$str_url = trim($str_url);

		$arr_wp_ud = wp_upload_dir();

		$str_uploads = str_replace( ABSPATH, '', $arr_wp_ud['basedir'] );
		$this->_str_uploads = $this->trimSlashes($str_uploads);

		$arr_parse_url = parse_url( $str_url );
		if ( is_array( $arr_parse_url ) ) {

			if ( isset( $arr_parse_url['scheme'] ) ) {
				$this->_str_scheme = $arr_parse_url['scheme'];
			}
			if ( isset( $arr_parse_url['host'] ) ) {
				$this->_str_host = $arr_parse_url['host'];
			}
			if ( isset( $arr_parse_url['query'] ) ) {
				$this->_str_query = $arr_parse_url['query'];
			}
			if ( isset( $arr_parse_url['fragment'] ) ) {
				$this->_str_fragment = $arr_parse_url['fragment'];
			}
			if ( isset( $arr_parse_url['path'] ) ) {

				$str_file_ext = basename( $arr_parse_url['path'] );

				$arr_pathinfo = pathinfo($str_file_ext);
				if ( isset($arr_pathinfo['filename']) ){
					$this->_str_filename = $arr_pathinfo['filename'];
				}
				if ( isset($arr_pathinfo['extension']) ){
					$this->_str_extension = $arr_pathinfo['extension'];
				}

				// remove the file.ext
				$str_subdir = str_replace( $str_file_ext, '', $arr_parse_url['path'] );
				// remove the uploads folder bit
				$str_subdir = str_replace( $this->_str_uploads, '', $str_subdir );
				// voila - the subdie :)
				$this->_str_uploads_subdir = $this->trimSlashes($str_subdir);

			}
		}


	}

	// TODO - is there a better way to do this?
	protected function trimSlashes( $str = ''){

		$str = ltrim($str, '/');
		$str = rtrim($str, '/');
		return $str;
	}


	public function __get( $str_prop ) {

		$str_prop = trim( strtolower( $str_prop ) );

		switch ( $str_prop ) {

			case 'abspath':
				return $this->trimSlashes(ABSPATH);

			case 'scheme':
				return $this->_str_scheme;

			case 'host':
				return $this->_str_host;

			case 'uploads':
				return $this->_str_uploads;

			case 'uploads_subdir':
				return $this->_str_uploads_subdir;

			case 'filename':
				return $this->_str_filename;

			case 'extension':
				return $this->_str_extension;

			case 'query':
				return $this->_str_query;

			case 'fragment':
				return $this->_str_fragment;

			case 'url':
				return $this->_str_url;

			default:
				return false;

		}
	}

}